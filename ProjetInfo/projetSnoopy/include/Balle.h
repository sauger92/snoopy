#ifndef BALLE_H_INCLUDED
#define BALLE_H_INCLUDED
#include "console.h"
#include <conio.h>

class Balle
{
    private: ///attributs
        char m_lettre; //lettre � afficher pour une balle
        int m_xposition, m_yposition; // position selon x et y
        int m_dep_x, m_dep_y; // d�placement selon x et y

    public: ///constructeurs et destructeurs
        Balle(); //par d�faut
        Balle( char _lettre, int _xposition, int _yposition, int _dep_x, int _dep_y); //surchag�
        ~Balle(); //destructeur

        char getLettre() const; /// getters
        int getXposition() const;
        int getYposition() const;
        int getDep_x() const;
        int getDep_y() const;
        void setLettre(char lettre); ///setters
        void setXposition(int xposition);
        void setYposition(int yposition);
        void setDep_x(int dep_x);
        void setDep_y(int dep_y);

        void AffichageBalle(Console* pConsole); ///proc�dure pour afficher les balles
        //int Deplacement() /// fonction qui g�re le d�placement des balles
};

#endif // BALLE_H_INCLUDED
