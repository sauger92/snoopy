#ifndef MATRICE_H
#define MATRICE_H
#include <vector>
#include "Oiseau.h"
#include "Personnage.h"
#include "Bloc.h"
#include "Balle.h"

using namespace std;

class Matrice
{
    public:
        Matrice();
        Matrice(int nbOiseau, int nbBloc);
        vector <Oiseau> getTabOiseau();
        vector <Bloc> getTabT();
        vector <Bloc> getTabP();
        vector <Bloc> getTabC();
        vector <Balle> getTabBalle();
        void setTabT(int j, int depx, int depy);
        void setTabP(int j, int depx, int depy);
        void setTabC(int j, int depx, int depy);
        void setTabBalle(int j, int depx, int depy);
        void setnbBlocT(int nb);
        void setnbBlocP(int nb);
        void setnbBlocC(int nb);
        void setnbBalle(int nb);
        char getMatrice(int i, int j);
        void setMatrice (int i, int j, char valeur);
        int getnbBlocT() const;
        int getnbBlocP() const;
        int getnbBlocC() const;
        int getnbBalle() const;
        int Coord(int i, int valeur, int niveau, char type);
        int CoordBalle(int i, int valeur, int niveau);


        void AffichageMat(Console* pConsole);
        void DeplacementSnoopy(Console* pConsole, int key, int* nbOiseau, int* nbvie, bool crea);
        char DeplacementSnoopy2(Console* pConsole, int key ,char tmp2);
        void DeplacementBalle(Console* pConsole);
        void CollisionOiseau(int x, int y, int* nbOiseau);
        void CollisionBlocT(int x, int y, int* nbVie);
        void CollsionBlocC();
        void CollisionBlocP(int x, int y, int depx, int depy);
        void CollisionBlocP2(int x, int y, int depx, int depy);
        bool CollisionBalle(bool choc);

        virtual ~Matrice();
    protected:
    private:
        vector <Oiseau> m_tabOiseau;
        vector <Bloc> m_tabT;
        vector <Bloc> m_tabP;
        vector <Bloc> m_tabC;
        vector <Balle> m_tabBalle;
        char m_Matrice[10] [20];
        Personnage m_snoopy;
        Balle m_balle;
        int m_nbBlocT;
        int m_nbBalle;
        int m_nbBlocP;
        int m_nbBlocC;

};

#endif // MATRICE_H
