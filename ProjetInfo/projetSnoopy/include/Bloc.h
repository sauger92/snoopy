#ifndef BLOC_H_INCLUDED
#define BLOC_H_INCLUDED
class Bloc
{
private:

    char m_type; ///Type du bloc: 1. cassable  2. d�pla�able  3. pi�g� . Et donc lettre correspondante.
    int m_Xposition; ///Abscisse du bloc.
    int m_Yposition; ///Ordonn�e du bloc.
    bool m_dep;

public:

    ///Getter and Setter

    void setXposition(int abscisse); ///D�termine l'abscisse du bloc.
    int getXposition() const; ///Renvoie l'abscisse du bloc.

    void setYposition(int ordonnee); ///D�termine l'ordonn�e du bloc.
    int getYposition() const; ///Renvoie l'ordonnee du bloc.

    void setType(char type); ///D�termine le type du bloc.
    char getType() const; ///Renvoie le type du bloc.

    void setDep(bool a);///D�termine si un bloc Pa d�j� �t� d�plac� ou non
    bool getDep() const;///Renvoie le bool�en concernant le d�placement

    ///Constructor and Destructor

    Bloc(char type, int x, int y);///Initialise les valeurs par d�faut.
    ~Bloc();



};



#endif // BLOC_H_INCLUDED
