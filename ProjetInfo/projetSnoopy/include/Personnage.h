#ifndef PERSONNAGE_H_INCLUDED
#define PERSONNAGE_H_INCLUDED
#include"console.h"


class Personnage
{
    private: /// attributs
    char m_lettre; //Lettre � afficher pour representer le personnage
    int m_vies; // Vies du personnage
    int m_pos_x; int m_pos_y; // Sa position selon x et y
    int m_dep_x; int m_dep_y; // Son d�placement selon x et y
    bool m_vivant; // bool�en true = personnage vivant et false =personnage mort

    public:  /// Constructeur
       Personnage(); // par d�faut
       Personnage( char _lettre, int _vies, int _pos_x, int _posy, int _dep_x, int _dep_y, bool _vivant); //surcharg�
       ~Personnage(); //destructeur

    void AffichagePersonnage(Console* pConsole); /// M�thode pour afficher la lettre du personnage

        char getLettre() const; /// getters
        int getVies() const;
        int getPos_x() const;
        int getPos_y() const;
        int getDep_x() const;
        int getDep_y() const;
        bool getVivant() const;
        void setLettre(char lettre); ///setters
        void setVies(int vies);
        void setPos_x(int pos_x);
        void setPos_y(int pos_y);
        void setDep_x(int dep_x);
        void setDep_y(int dep_y);
        void setVivant(bool vivant);

};

#endif // PERSONNAGE_H_INCLUDED
