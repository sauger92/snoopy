#ifndef OISEAU_H
#define OISEAU_H
#include "Console.h"


class Oiseau
{
    public:
        Oiseau();
        Oiseau(int positionX, int positionY);

        char getLettre() const;
        int getPositionX() const;
        int getPositionY() const;

        void setPositionX(int positionX);
        void setPositionY(int positionY);

        void AffichageOiseau(Console* pConsole);

        virtual ~Oiseau();
    protected:
    private:
        char m_lettre;
        int m_positionX;
        int m_positionY;

};

#endif // OISEAU_H
