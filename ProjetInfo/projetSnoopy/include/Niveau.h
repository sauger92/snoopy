#ifndef NIVEAU_H
#define NIVEAU_H
#include <iostream>
#include "Oiseau.h"
#include "console.h"
#include "Personnage.h"
#include "Balle.h"
#include "Matrice.h"
#include <cstdlib>
#include "Bloc.h"
#include "windows.h"
#include <fstream>
#include <ctime>
#include <time.h>


class Niveau
{
    public:
        Niveau();
        bool jouer(int niv, Matrice *mat, int nbOiseau, bool crea);
        bool horloge(Console* pConsole, bool tempsfini, int compteur);
        void sauvegarde(int niveau, Matrice mat, int nbOiseau, bool crea);
        void score(bool dernierNiveau, int niveau);
        void editNiveau();
        int menu();
        bool menu2();

        virtual ~Niveau();
    protected:
    private:
    char m_a;
    Console* m_pConsole;
    bool m_victoire;
    int m_nbOiseau;
    int* m_nbOiseau2;
    int m_nbVie;
    int* m_nbVie2;
    int m_timer;
    char m_vie [3];

};

#endif // NIVEAU_H
