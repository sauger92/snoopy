#include "console.h"
#include "Balle.h"

Balle::Balle()//Default constructor
    :m_lettre('B'),m_xposition(0),m_yposition(0),m_dep_x(1),m_dep_y(1)
{
}

Balle::Balle(char _lettre, int _xposition, int _yposition, int _dep_x, int _dep_y )//Overload constructor
    :m_lettre(_lettre), m_xposition(_xposition), m_yposition(_yposition), m_dep_x(_dep_x), m_dep_y(_dep_y)
{
}

Balle::~Balle()//Destructor
{
}

///Getter
char Balle::getLettre() const
{
    return m_lettre;
}
int Balle::getXposition() const
{
    return m_xposition;
}

int Balle::getYposition() const
{
    return m_yposition;
}

int Balle::getDep_x() const
{
    return m_dep_x;
}

int Balle::getDep_y() const
{
    return m_dep_y;
}

///Setter
void Balle::setLettre(char lettre)
{
    m_lettre = lettre;
}

void Balle::setXposition(int xposition)
{
    m_xposition = xposition;
}

void Balle::setYposition(int yposition)
{
    m_yposition = yposition;
}

void Balle::setDep_x(int dep_x)
{
    m_dep_x = dep_x;
}

void Balle::setDep_y(int dep_y)
{
    m_dep_y = dep_y;
}

///M�thodes
void Balle::AffichageBalle(Console* pConsole) // Proc�dure qui affiche la balle sur la console
{
    pConsole->gotoLigCol(getXposition(), getYposition()); // r�cup�re les coordonn�es
    pConsole->setColor(COLOR_RED); //choisi la couleur
    std::cout << getLettre(); //affiche
}

/*int Balle::DeplacementBalle()
{
    if
} */
