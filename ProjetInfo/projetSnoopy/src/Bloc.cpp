#include <iostream>
#include "Bloc.h"

using namespace std;

///Getters and Setters

void Bloc::setXposition(int abscisse)
{
    m_Xposition=abscisse;
}

int Bloc::getXposition()const
{
    return m_Xposition;
}

void Bloc::setYposition(int ordonnee)
{
    m_Yposition=ordonnee;
}

int Bloc::getYposition() const
{
    return m_Yposition;
}

void Bloc::setType(char type)
{
    m_type=type;
}

char Bloc::getType() const
{
    return m_type;
}

void Bloc::setDep(bool a)
{
    m_dep = a;
}

bool Bloc::getDep()const
{
    return m_dep;
}


///Constructor and Destructor

Bloc::Bloc(char type, int x, int y)

{
    m_type = type;///Type du bloc (T, C, P)
    m_Xposition = x;
    m_Yposition = y;
    m_dep = false;  ///le bloc n'a pas encore �t� d�plac�.
}

Bloc::~Bloc()
{

}
