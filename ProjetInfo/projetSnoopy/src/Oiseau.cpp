#include "Oiseau.h"

Oiseau::Oiseau()
{
    //ctor
}

Oiseau::Oiseau(int positionX, int positionY)
{
    m_lettre = 'O';
    m_positionX = positionX;
    m_positionY = positionY;
}

char Oiseau::getLettre() const
{
    return m_lettre;
}

int Oiseau::getPositionX() const
{
    return m_positionX;
}

int Oiseau::getPositionY() const
{
    return m_positionY;
}

void Oiseau::setPositionX(int positionX)
{
    m_positionX = positionX;
}

void Oiseau::setPositionY(int positionY)
{
    m_positionY = positionY;
}

void Oiseau::AffichageOiseau(Console* pConsole)
{
    pConsole->gotoLigCol(getPositionX(), getPositionY());
    pConsole->setColor(COLOR_BLUE);
    std::cout << getLettre();
}
Oiseau::~Oiseau()
{
    //dtor
}
