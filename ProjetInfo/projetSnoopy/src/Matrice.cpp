#include "Matrice.h"
#include <vector>
#include <iostream>
#include "Oiseau.h"
#include "Personnage.h"
#include "Bloc.h"
#include "Balle.h"
#include <ctime>
#include <time.h>
#include "console.h"
#include <fstream>

using namespace std;

Matrice::Matrice()
{
    //ctor
}
Matrice::Matrice(int nbOiseau, int niveau)///Constructeur de la matrice correpondant au niveau recu
{


    for (int i=0; i<10; i++)
    {
        for(int j=0; j<20; j++)
        {

            m_Matrice[i][j]=' ';
        }
    }
    for (int i = 0; i < 4; i++)//Initialisation des oiseaux aux 4 coins de la matrice
    {

        if (i == 0)
        {
            m_tabOiseau.push_back(Oiseau(0, 0));

        }
        if (i == 1)
        {
            m_tabOiseau.push_back(Oiseau(0, 19));
        }
        if (i == 2)
        {
            m_tabOiseau.push_back(Oiseau(9, 0));
        }
        if (i == 3)
        {
            m_tabOiseau.push_back(Oiseau(9, 19));
        }
    }

    if (niveau==1) // 1er niveau
    {
        setnbBlocT(7);
        setnbBalle(1);
        setnbBlocP(12);
        setnbBlocC(3);
    }
    if (niveau==2) // 2eme niveau
    {
        setnbBlocT(29);
        setnbBalle(1);
        setnbBlocP(4);
        setnbBlocC(7);
    }

    if (niveau==3) // 3eme niveau
    {
        setnbBlocT(4);
        setnbBalle(1);
        setnbBlocP(25);
        setnbBlocC(7);
    }




    for (int l = 0; l < m_nbBlocT; l++)///initialisation des Blocs � l'aide des coordon�es contenu dans la fonction Coord
    {

        m_tabT.push_back(Bloc('T', Coord(l, 0, niveau,'T'), Coord(l, 1, niveau, 'T')));

    }
    for(int l=0; l<m_nbBlocP; l++ )
    {

        m_tabP.push_back(Bloc('P', Coord(l, 0, niveau,'P'), Coord(l, 1, niveau, 'P')));

    }
    for(int l=0; l<m_nbBlocC; l++ )
    {

        m_tabC.push_back(Bloc('C', Coord(l, 0, niveau,'C'), Coord(l, 1, niveau, 'C')));

    }
    /*for (int x=0; x < m_nbBalle; x++)
    {
        m_tabBalle.push_back(Balle('B', CoordBalle(x, 0, 1), CoordBalle(x, 1, 1), 1, 1));
    }*/
    ///Placement de la balle
    m_balle.setDep_x(1);
    m_balle.setDep_y(1);
    m_balle.setXposition(9);
    m_balle.setYposition(7);


///On initialise la matrice
    for(int i=0; i<10; i++)
    {
        for(int j=0; j<20; j++)
        {
            if((m_snoopy.getPos_x()==i)&&(m_snoopy.getPos_y()==j))
            {
                m_Matrice[i][j]='S';
            }
            else
            {
                for(int k=0; k<4; k++)
                {
                    if ((m_tabOiseau[k].getPositionX() == i)&&(m_tabOiseau[k].getPositionY()== j))m_Matrice[i][j] = 'O';

                }
                for(int m=0; m<getnbBlocT(); m++)
                {
                    if ((m_tabT[m].getXposition() == i)&&(m_tabT[m].getYposition() == j))m_Matrice[i][j]='T';

                }
                for(int m=0; m<getnbBlocP(); m++)
                {
                    if ((m_tabP[m].getXposition() == i)&&(m_tabP[m].getYposition() == j))m_Matrice[i][j]='P';

                }
                for(int m=0; m<getnbBlocC(); m++)
                {
                    if ((m_tabC[m].getXposition() == i)&&(m_tabC[m].getYposition() == j))m_Matrice[i][j]='C';

                }

                if ((m_balle.getXposition() == i)&&(m_balle.getYposition() == j))m_Matrice[i][j]='B';


            }

        }
    }
}

vector <Oiseau> Matrice::getTabOiseau()
{
    return m_tabOiseau;
}



int Matrice::CoordBalle(int i, int valeur, int niveau)
{
///Niveau 1 : balle

    int tab_T1[4][2];

    tab_T1[0][0]=3;
    tab_T1[0][1]=4;


    if (niveau==1)
    {
        return tab_T1[i][valeur];
    }
    //return tab_T1[i][valeur];
}

vector <Bloc> Matrice::getTabT()
{
    return m_tabT;
}

vector <Bloc> Matrice::getTabP()
{
    return m_tabP;
}

vector <Bloc> Matrice::getTabC()
{
    return m_tabC;
}

vector <Balle> Matrice::getTabBalle()
{
    return m_tabBalle;
}

char Matrice::getMatrice(int i, int j)
{
    return m_Matrice[i][j];
}

int Matrice::getnbBlocC() const
{
    return m_nbBlocC;
}
int Matrice::getnbBlocT() const
{
    return m_nbBlocT;
}
int Matrice::getnbBlocP() const
{
    return m_nbBlocP;
}
int Matrice::getnbBalle() const
{
    return m_nbBalle;
}
void Matrice::setMatrice(int i, int j, char valeur)
{
    m_Matrice[i][j] = valeur;
}

void Matrice::setTabT(int i,int depx, int depy)
{
    m_tabT[i].setXposition(m_tabT[i].getXposition()+depx);
    m_tabT[i].setYposition(m_tabT[i].getYposition()+depy);
}
void Matrice::setTabP(int i,int depx, int depy)
{
    m_tabP[i].setXposition(m_tabP[i].getXposition()+depx);
    m_tabP[i].setYposition(m_tabP[i].getYposition()+depy);
}
void Matrice::setTabC(int i,int depx, int depy)
{
    m_tabC[i].setXposition(m_tabC[i].getXposition()+depx);
    m_tabC[i].setYposition(m_tabC[i].getYposition()+depy);
}

void Matrice::setTabBalle(int i,int depx, int depy)
{
    m_tabBalle[i].setXposition(m_tabBalle[i].getXposition()+depx);
    m_tabBalle[i].setYposition(m_tabBalle[i].getYposition()+depy);
}

void Matrice::setnbBlocC(int nb)
{
    m_nbBlocC=nb;
}
void Matrice::setnbBlocP(int nb)
{
    m_nbBlocP=nb;
}
void Matrice::setnbBlocT(int nb)
{
    m_nbBlocT=nb;
}

void Matrice::setnbBalle(int nb)
{
    m_nbBalle = nb;
}

void Matrice::AffichageMat(Console* pConsole)///Affichage de la matrice et r�partition des couleurs entre les diff�rents Blocs, personnage, balle,...
{
    vector <Oiseau>::iterator it;
    for (int i=0; i<10; i++)
    {
        for(int j=0; j<20; j++)
        {
            if(getMatrice(i, j)=='O')pConsole->setColor(COLOR_YELLOW);
            if(getMatrice(i, j)=='T')pConsole->setColor(COLOR_BLUE);
            if(getMatrice(i, j)=='P')pConsole->setColor(COLOR_GREEN);
            if(getMatrice(i, j)=='B')pConsole->setColor(COLOR_RED);
            if(getMatrice(i, j)=='C')pConsole->setColor(COLOR_ROSE);
            if(getMatrice(i, j)=='x')pConsole->setColor(COLOR_KAKI);
            cout<<getMatrice(i, j);
            pConsole->setColor(COLOR_WHITE);

        }
        cout <<endl;
        //cout <<CoordT(i, 0, 1)<<endl;
    }

}

void Matrice::CollisionBlocT(int x, int y, int* nbVie)
{
    vector<Bloc>::iterator it;
    for (it=getTabT().begin(); it!=getTabT().end(); it++)
    {
        if(((*it).getXposition()==x)&&((*it).getYposition()==y))
        {
            setMatrice(x, y, ' ');
        }
    }

}

void Matrice::DeplacementSnoopy(Console* pConsole, int key, int* nbOiseau, int* nbVie, bool crea)
{
    char tmp;
    if (key == 'C' || key =='c')///La touche C permet de d�truire les Blocs C a proximit�
    {
        CollsionBlocC();
    }
    //si touche deplacement haut
    if (key == 'i'|| key=='I')
    {
        if (m_snoopy.getPos_x()!=0)
        {

            tmp = getMatrice(m_snoopy.getPos_x()-1, m_snoopy.getPos_y());///On s'int�resse au prochaine emplacement de Snoopy pour les collisions avec les differents acteurs.
            if (tmp != 'P'&&tmp!='C')
            {
                if (tmp == 'O')
                {
                    CollisionOiseau(m_snoopy.getPos_x()-1, m_snoopy.getPos_y(),  nbOiseau);///On regarde le nombre d'oiseaux restants
                    tmp =' ';
                }
                if (tmp=='T')///Une collision avec un Bloc T faire perdre une vie au Snoopy
                {
                    //CollisionBlocT(m_snoopy.getPos_x()-1, m_snoopy.getPos_y(), nbVie);
                    (*nbVie)=(*nbVie)-1;

                    tmp=' ';
                }
                if (tmp=='B')/// les contacts avec la balle sont g�r�s en permance m�me sans mouvement de Snoopy
                {
                    tmp=' ';

                }
                ///On effectue un mouvement de Snoopy dans la direction souhait�
                setMatrice(m_snoopy.getPos_x()-1, m_snoopy.getPos_y(), m_snoopy.getLettre());
                setMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y(), tmp);
                m_snoopy.setPos_x(m_snoopy.getPos_x()-1);
            }
            else
            {
                if(tmp == 'P')
                {
                    if(crea ==true) CollisionBlocP2(m_snoopy.getPos_x()-1, m_snoopy.getPos_y(), -1, 0);
                    else CollisionBlocP(m_snoopy.getPos_x()-1, m_snoopy.getPos_y(), -1, 0);
                }
            }
        }
    }

    //si deplacement gauche
    if (key == 'j'|| key=='J')
    {
        if(m_snoopy.getPos_y()!=0)
        {

            tmp = getMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y()-1);
            if (tmp!='P'&&tmp!='C')
            {
                if (tmp == 'O')
                {
                    CollisionOiseau(m_snoopy.getPos_x(), m_snoopy.getPos_y()-1, nbOiseau);
                    tmp =' ';


                }
                if (tmp=='T')
                {
                    //CollisionBlocT(m_snoopy.getPos_x(), m_snoopy.getPos_y()-1, nbVie);
                    tmp=' ';
                    (*nbVie)=(*nbVie)-1;
                }
                if (tmp=='B')
                {
                    //CollisionBalle(m_snoopy.getPos_x(), m_snoopy.getPos_y()-1, nbVie);
                    tmp=' ';
                    //(*nbVie)=(*nbVie)-1;
                }


                setMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y()-1, m_snoopy.getLettre());
                setMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y(), tmp);
                m_snoopy.setPos_y(m_snoopy.getPos_y()-1);
            }
            else
            {
                if(tmp == 'P')
                {
                    if (crea==true)CollisionBlocP(m_snoopy.getPos_x(), m_snoopy.getPos_y()-1, 0, -1);
                    else CollisionBlocP(m_snoopy.getPos_x(), m_snoopy.getPos_y()-1, 0, -1);
                }

            }
        }

    }

    //si touche deplacement droite
    if (key == 'l'|| key == 'L')
    {
        if(m_snoopy.getPos_y()!=19)
        {

            tmp = getMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y()+1);
            if (tmp!='P'&&tmp!='C')
            {
                if (tmp == 'O')
                {
                    CollisionOiseau(m_snoopy.getPos_x(), m_snoopy.getPos_y()+1, nbOiseau);
                    tmp =' ';


                }
                if (tmp=='T')
                {
                    //CollisionBlocT(m_snoopy.getPos_x(), m_snoopy.getPos_y()+1, nbVie);
                    tmp=' ';
                    (*nbVie)=(*nbVie)-1;
                }
                if (tmp=='B')
                {
                    //CollisionBalle(m_snoopy.getPos_x(), m_snoopy.getPos_y()+1, nbVie);
                    tmp=' ';
                    //(*nbVie)=(*nbVie)-1;
                }


                setMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y()+1, m_snoopy.getLettre());
                setMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y(), tmp);
                m_snoopy.setPos_y(m_snoopy.getPos_y()+1);
            }
            else
            {
                if(tmp == 'P')
                {
                    if (crea==true)CollisionBlocP2(m_snoopy.getPos_x(), m_snoopy.getPos_y()+1, 0, +1);
                    else CollisionBlocP(m_snoopy.getPos_x(), m_snoopy.getPos_y()+1, 0, +1);
                }

            }

        }

    }

    //touche bas
    if (key=='k'|| key == 'K')
    {
        if (m_snoopy.getPos_x()!=9)
        {

            tmp = getMatrice(m_snoopy.getPos_x()+1, m_snoopy.getPos_y());
            if (tmp!='P'&&tmp!='C')
            {
                if (tmp == 'O')
                {
                    CollisionOiseau(m_snoopy.getPos_x()+1, m_snoopy.getPos_y(), nbOiseau);
                    tmp =' ';
                }
                if (tmp=='T')
                {
                    //CollisionBlocT(m_snoopy.getPos_x()+1, m_snoopy.getPos_y(), nbVie);
                    tmp=' ';
                    (*nbVie)=(*nbVie)-1;
                }
                if (tmp=='B')
                {
                    //qCollisionBalle(m_snoopy.getPos_x()+1, m_snoopy.getPos_y(), nbVie);
                    tmp=' ';
                    //(*nbVie)=(*nbVie)-1;

                }

                setMatrice(m_snoopy.getPos_x()+1, m_snoopy.getPos_y(), m_snoopy.getLettre());
                setMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y(), tmp);
                m_snoopy.setPos_x(m_snoopy.getPos_x()+1);
            }
            else
            {
                if (tmp=='P')
                {
                    if (crea==true)CollisionBlocP2(m_snoopy.getPos_x()+1, m_snoopy.getPos_y(), 1, 0);
                    else CollisionBlocP(m_snoopy.getPos_x()+1, m_snoopy.getPos_y(), 1, 0);
                }
            }

        }
    }
}
bool Matrice::CollisionBalle(bool choc)///Fonction de Contact entre Snoopy et la Balle
{
    if(m_balle.getXposition()==m_snoopy.getPos_x()&&m_balle.getYposition()==m_snoopy.getPos_y())choc = true;
    else choc = false;

    return choc;
}

char Matrice::DeplacementSnoopy2(Console* pConsole, int key, char tmp2)///d�placement du Snoopy dans le mode Snoopy le Constructeur
{
    char tmp;

    if (key == 'P'|| key =='p')
    {
        setMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y(), 'P');
        tmp2 = 'P';
    }
    if (key=='C'||key == 'c')
    {
        setMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y(), 'C');
        tmp2 = 'C';
    }
    if (key == 'T'|| key =='t')
    {
        setMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y(), 'T');
        tmp2 = 'T';
    }
    //si touche deplacement haut

    if (key == 'i'|| key=='I')
    {
        if (m_snoopy.getPos_x()!=0)
        {
            if (tmp2 == 'A')tmp = getMatrice(m_snoopy.getPos_x()-1, m_snoopy.getPos_y());
            else
            {
                tmp = tmp2;
                tmp2 = 'A';

            }


            if (tmp != 'O'&&tmp!='B')
            {
                setMatrice(m_snoopy.getPos_x()-1, m_snoopy.getPos_y(), m_snoopy.getLettre());
                setMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y(), tmp);
                m_snoopy.setPos_x(m_snoopy.getPos_x()-1);
            }

        }
    }

    //si deplacement gauche
    if (key == 'j'|| key=='J')
    {
        if(m_snoopy.getPos_y()!=0)
        {

            if (tmp2 == 'A')tmp = getMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y()-1);
            else
            {
                tmp = tmp2;
                tmp2 = 'A';

            }
            if (tmp!='O'&&tmp!='B')
            {
                setMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y()-1, m_snoopy.getLettre());
                setMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y(), tmp);
                m_snoopy.setPos_y(m_snoopy.getPos_y()-1);
            }
        }

    }

    //si touche deplacement droite
    if (key == 'l'|| key == 'L')
    {
        if(m_snoopy.getPos_y()!=19)
        {


            if (tmp2 == 'A')tmp = getMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y()+1);
            else
            {
                tmp = tmp2;
                tmp2 = 'A';

            }


            setMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y()+1, m_snoopy.getLettre());
            setMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y(), tmp);
            m_snoopy.setPos_y(m_snoopy.getPos_y()+1);
        }

    }



    //touche bas
    if (key=='k'|| key == 'K')
    {
        if (m_snoopy.getPos_x()!=9)
        {


            if (tmp2 == 'A')tmp = getMatrice(m_snoopy.getPos_x()+1, m_snoopy.getPos_y());
            else
            {
                tmp = tmp2;
                tmp2 = 'A';

            }

            setMatrice(m_snoopy.getPos_x()+1, m_snoopy.getPos_y(), m_snoopy.getLettre());
            setMatrice(m_snoopy.getPos_x(), m_snoopy.getPos_y(), tmp);
            m_snoopy.setPos_x(m_snoopy.getPos_x()+1);
        }

    }
    return tmp2;

}

void Matrice::DeplacementBalle(Console* pConsole)///d�placement de la balle
{
    char tmp;
    ///On blinde le d�placement pour �viter une sortie de la matrice
    if(m_balle.getXposition()>=9)m_balle.setDep_x(-1);
    if(m_balle.getXposition()<=0)m_balle.setDep_x(1);
    if(m_balle.getYposition()>=19)m_balle.setDep_y(-1);
    if(m_balle.getYposition()<=0)m_balle.setDep_y(1);

    ///on emp�che les contactes entre la balle et un bloc ou un oiseau
    if (m_balle.getXposition()+m_balle.getDep_x()<=9 && m_balle.getXposition()+m_balle.getDep_x()>=0)
    {
        if( m_balle.getYposition()+m_balle.getDep_y()>=0 && m_balle.getYposition()+m_balle.getDep_y()<=19)
        {


            tmp = getMatrice(m_balle.getXposition()+m_balle.getDep_x(), m_balle.getYposition()+m_balle.getDep_y());
            if (tmp != ' ' && tmp != 'S')
            {
                //if (m_balle.getDep_x()==-1)m_balle.setDep_x(1);
                //m_balle.setDep_x(-m_balle.getDep_x());
                m_balle.setDep_y(-m_balle.getDep_y());
                char tmp2 = getMatrice(m_balle.getXposition()+m_balle.getDep_x(), m_balle.getYposition()+m_balle.getDep_y());
                if (tmp2!= ' ')m_balle.setDep_x(-m_balle.getDep_x());


            }
        }
    }
    ///d�placement de la Balle
    setMatrice(m_balle.getXposition(), m_balle.getYposition(), ' ');
    m_balle.setXposition(m_balle.getXposition()+m_balle.getDep_x());
    m_balle.setYposition(m_balle.getYposition()+m_balle.getDep_y());
    setMatrice(m_balle.getXposition(), m_balle.getYposition(), 'B');


}

void Matrice::CollisionOiseau(int x, int y, int* nbOiseau)
{

    (*nbOiseau)= (*nbOiseau)-1;
    ///calcul du nombre d'oiseau restant pour l'affichage niveau

}
void Matrice:: CollsionBlocC()
{
///suppression des blocs C pr�sent au dessus, en dessous, � gauche, � droite du Snoopy
    for (int i=0; i<10; i++)
    {
        for(int j=0; j<20; j++)
        {
            if (getMatrice(i,j)=='C')
            {
                if (i==(m_snoopy.getPos_x()+1)||(i==m_snoopy.getPos_x()-1))
                {
                    if ((j==(m_snoopy.getPos_y()))||(j==m_snoopy.getPos_y()))
                        setMatrice(i,j, ' ');

                }


                if ((i==(m_snoopy.getPos_x()))||(i==(m_snoopy.getPos_x())))
                {
                    if ((j==(m_snoopy.getPos_y()+1))||(j==(m_snoopy.getPos_y())-1))
                    {
                        setMatrice(i,j, ' ');
                    }
                }
            }
        }
    }
}

void Matrice::CollisionBlocP(int x, int y, int depx, int depy)///D�placement d'un bloc P pouss� par Snoopy
{
    for (int i=0; i<getnbBlocP(); i++)
    {
        if((getTabP()[i].getXposition()==x)&&(getTabP()[i].getYposition()==y))
        {
            if (getTabP()[i].getDep()==false && getMatrice(x+depx, y+depy)==' ')
            {
                if ((x+depx<=9)&&(x+depx>=0)&&(y+depy>=0)&&(y+depy<=19))
                {
                    setMatrice(x, y, ' ');
                    setMatrice(x+depx, y+depy, 'P');
                    getTabP()[i].setDep(true);///permet que les blocs ne soient poussables qu'une seule fois
                }

            }
        }

    }
}
void Matrice::CollisionBlocP2(int x, int y, int depx, int depy)
{

    ///Pour le mode Snoopy le constructeur, les bloc P sont poussables ind�finiement tant que ceux-ci ne sortent pas de la Matrice
    for(int i=0; i<10; i++)
    {
        for(int j=0; j<20; j++)
        {
            if(getMatrice(i,j)=='P')
            {
                if((i==x)&&(j==y))
                {
                    if (getMatrice(x+depx, y+depy)==' ')
                    {
                        if ((x+depx<=9)&&(x+depx>=0)&&(y+depy>=0)&&(y+depy<=19))
                        {
                            setMatrice(x, y, ' ');
                            setMatrice(x+depx, y+depy, 'P');
                            //getTabP()[i].setDep(true);
                        }
                    }
                }
            }
        }
    }
}



int Matrice::Coord(int i, int valeur, int niveau, char type)///Fonction r�pertoire des coordonn�es des blocs des 3 niveaux
{
///Niveau 1

///Bloc T

    int tab_T1[7][2];

    tab_T1[0][0]=5;
    tab_T1[0][1]=9;

    tab_T1[1][0]=6;
    tab_T1[1][1]=10;

    tab_T1[2][0]=5;
    tab_T1[2][1]=11;

    tab_T1[3][0]=8;
    tab_T1[3][1]=1;


    tab_T1[4][0]=9;
    tab_T1[4][1]=1;

    tab_T1[5][0]=0;
    tab_T1[5][1]=18;

    tab_T1[6][0]=1;
    tab_T1[6][1]=18;

    ///Bloc P(poussable)

    int tab_P1[12][2];

    tab_P1[0][0]=0;
    tab_P1[0][1]=2;

    tab_P1[1][0]=1;
    tab_P1[1][1]=2;

    tab_P1[2][0]=2;
    tab_P1[2][1]=1;

    tab_P1[3][0]=2;
    tab_P1[3][1]=0;

    tab_P1[4][0]=2;
    tab_P1[4][1]=3;

    tab_P1[5][0]=3;
    tab_P1[5][1]=2;

    tab_P1[6][0]=7;
    tab_P1[6][1]=1;

    tab_P1[7][0]=2;
    tab_P1[7][1]=18;

    tab_P1[8][0]=9;
    tab_P1[8][1]=17;

    tab_P1[9][0]=8;
    tab_P1[9][1]=17;

    tab_P1[10][0]=7;
    tab_P1[10][1]=18;

    tab_P1[11][0]=7;
    tab_P1[11][1]=19;

    ///Bloc C(cassable)

    int tab_C1[1][2];
    tab_C1[0][0]=4;
    tab_C1[0][1]=10;
    ///Niveau 2
    ///Bloc P

    int tab_P2[4][2];

    tab_P2[0][0]=7;
    tab_P2[0][1]=0;

    tab_P2[1][0]=7;
    tab_P2[1][1]=1;

    tab_P2[2][0]=8;
    tab_P2[2][1]=2;

    tab_P2[3][0]=9;
    tab_P2[3][1]=2;

    ///Niveau 2

    ///Bloc T

    int tab_T2[29][2];

    tab_T2[0][0]=1;
    tab_T2[0][1]=19;

    tab_T2[1][0]=1;
    tab_T2[1][1]=18;

    tab_T2[2][0]=1;
    tab_T2[2][1]=17;

    tab_T2[3][0]=1;
    tab_T2[3][1]=16;


    tab_T2[4][0]=2;
    tab_T2[4][1]=16;

    tab_T2[5][0]=2;
    tab_T2[5][1]=18;

    tab_T2[6][0]=3;
    tab_T2[6][1]=18;

    tab_T2[7][0]=4;
    tab_T2[7][1]=18;

    tab_T2[8][0]=4;
    tab_T2[8][1]=17;

    tab_T2[8][0]=4;
    tab_T2[8][1]=16;

    tab_T2[9][0]=0;
    tab_T2[9][1]=14;

    tab_T2[10][0]=1;
    tab_T2[10][1]=14;

    tab_T2[11][0]=2;
    tab_T2[11][1]=14;

    tab_T2[12][0]=3;
    tab_T2[12][1]=14;

    tab_T2[13][0]=4;
    tab_T2[13][1]=14;

    tab_T2[14][0]=4;
    tab_T2[14][1]=13;

    tab_T2[15][0]=6;
    tab_T2[15][1]=13;

    tab_T2[16][0]=6;
    tab_T2[16][1]=14;

    tab_T2[17][0]=6;
    tab_T2[17][1]=15;

    tab_T2[18][0]=6;
    tab_T2[18][1]=16;

    tab_T2[19][0]=7;
    tab_T2[19][1]=16;

    tab_T2[20][0]=8;
    tab_T2[20][1]=16;

    tab_T2[21][0]=9;
    tab_T2[21][1]=16;

    tab_T2[22][0]=6;
    tab_T2[22][1]=17;

    tab_T2[23][0]=6;
    tab_T2[23][1]=19;

    tab_T2[24][0]=7;
    tab_T2[24][1]=19;

    tab_T2[25][0]=8;
    tab_T2[25][1]=19;

    tab_T2[26][0]=8;
    tab_T2[26][1]=18;

    tab_T2[27][0]=1;
    tab_T2[27][1]=2;

    tab_T2[28][0]=2;
    tab_T2[28][1]=1;

    ///Niveau 2
    ///Bloc C

    int tab_C2[7][2];

    tab_C2[0][0]=0;
    tab_C2[0][1]=18;

    tab_C2[1][0]=4;
    tab_C2[1][1]=10;

    tab_C2[2][0]=5;
    tab_C2[2][1]=11;

    tab_C2[3][0]=6;
    tab_C2[3][1]=10;

    tab_C2[4][0]=5;
    tab_C2[4][1]=9;

    tab_C2[5][0]=0;
    tab_C2[5][1]=2;

    tab_C2[6][0]=2;
    tab_C2[6][1]=0;

    ///Niveau 3
///Bloc P

    int tab_P3[25][2];

    tab_P3[0][0]=0;
    tab_P3[0][1]=15;

    tab_P3[1][0]=1;
    tab_P3[1][1]=16;

    tab_P3[2][0]=2;
    tab_P3[2][1]=17;

    tab_P3[3][0]=3;
    tab_P3[3][1]=18;

    tab_P3[4][0]=4;
    tab_P3[4][1]=19;

    tab_P3[5][0]=7;
    tab_P3[5][1]=19;

    tab_P3[6][0]=7;
    tab_P3[6][1]=18;

    tab_P3[7][0]=8;
    tab_P3[7][1]=17;

    tab_P3[8][0]=9;
    tab_P3[8][1]=17;

    tab_P3[9][0]=4;
    tab_P3[9][1]=11;

    tab_P3[10][0]=5;
    tab_P3[10][1]=11;

    tab_P3[11][0]=6;
    tab_P3[11][1]=11;

    tab_P3[12][0]=6;
    tab_P3[12][1]=10;

    tab_P3[13][0]=6;
    tab_P3[13][1]=9;

    tab_P3[14][0]=5;
    tab_P3[14][1]=9;

    tab_P3[15][0]=4;
    tab_P3[15][1]=9;

    tab_P3[16][0]=4;
    tab_P3[16][1]=10;

    tab_P3[17][0]=0;
    tab_P3[17][1]=2;

    tab_P3[18][0]=1;
    tab_P3[18][1]=2;

    tab_P3[19][0]=2;
    tab_P3[19][1]=0;

    tab_P3[20][0]=2;
    tab_P3[20][1]=1;

    tab_P3[21][0]=7;
    tab_P3[21][1]=0;

    tab_P3[22][0]=7;
    tab_P3[22][1]=1;

    tab_P3[23][0]=8;
    tab_P3[23][1]=2;

    tab_P3[24][0]=9;
    tab_P3[24][1]=2;

    ///Niveau 3
    ///Bloc T

    int tab_T3[4][2];


    tab_T3[0][0]=3;
    tab_T3[0][1]=11;

    tab_T3[1][0]=3;
    tab_T3[1][0]=7;

    tab_T3[2][0]=5;
    tab_T3[2][1]=7;

    tab_T3[3][0]=6;
    tab_T3[3][1]=8;


    ///Niveau 3
    ///Bloc C

    int tab_C3[7][2];

    tab_C3[0][0]=2;
    tab_C3[0][1]=2;

    tab_C3[1][0]=2;
    tab_C3[1][1]=3;

    tab_C3[2][0]=3;
    tab_C3[2][1]=2;

    tab_C3[3][0]=7;
    tab_C3[3][1]=17;

    tab_C3[4][0]=2;
    tab_C3[4][1]=19;

    tab_C3[5][0]=1;
    tab_C3[5][1]=18;

    tab_C3[6][0]=0;
    tab_C3[6][1]=17;

    if ((niveau==1)&&(type=='T'))
    {
        return tab_T1[i][valeur];
    }

    if((niveau==1)&&(type=='P'))
    {
        return tab_P1[i][valeur];
    }

    if((niveau==1)&&(type=='C'))
    {
        return tab_C1[i][valeur];
    }
    if ((niveau==2)&&(type=='T'))
    {
        return tab_T2[i][valeur];
    }

    if((niveau==2)&&(type=='P'))
    {
        return tab_P2[i][valeur];
    }

    if((niveau==2)&&(type=='C'))
    {
        return tab_C2[i][valeur];
    }

    if ((niveau==3)&&(type=='T'))
    {
        return tab_T3[i][valeur];
    }

    if((niveau==3)&&(type=='P'))
    {
        return tab_P3[i][valeur];
    }

    if((niveau==3)&&(type=='C'))
    {
        return tab_C3[i][valeur];
    }


    //return tab_T1[i][valeur];

}


Matrice::~Matrice()
{
    //dtor
}
