#include "Niveau.h"
#include <string>
using namespace std;

Niveau::Niveau()///Constructeur du bloc
{
    m_a = 3;

    m_nbOiseau = 4;
    m_nbOiseau2 = &m_nbOiseau;
    m_nbVie = 3;
    m_nbVie2 = &m_nbVie;
    m_victoire = 0;
    m_timer = 0;
    for(int i=0; i<3; i++)
    {
        m_vie[i]=' ';
    }

    //ctor
}
bool Niveau::jouer(int niv, Matrice* mat, int nbOiseau, bool crea)
{
    /// fonction qui g�re un niveau et retourne un bool�en qui est vrai si le joueur a gagn�
    m_nbOiseau = 4;
    m_nbOiseau = nbOiseau;
    m_nbVie = 3;
    int cpt3 = 0;
    bool cpt2 = false;
    int nb = 0;
    Matrice mat1;
    bool choc =false;
    //clock_t clock(void);
    m_pConsole = NULL;
    m_pConsole = Console::getInstance(); // Alloue la m�moire du pointeur pConsole
    //if (mat == NULL)
    mat1  = Matrice(4 , niv);
    if (mat!=NULL)
    {
        for (int i=0; i<10; i++)
        {
            for(int j=0; j<20; j++)
            {
                mat1.setMatrice(i, j, mat->getMatrice(i, j));
            }
        }
    }

    system("cls");

    bool quit = true;
    bool tempsfini=false;
    int compteur = ( (int) clock()/CLOCKS_PER_SEC);//stocke le temps �coul� du lancement du jeu jusqu'au au d�but du niveau
    horloge(m_pConsole, tempsfini, compteur);
    int cmp = 0;
    int cmp2 = 0;

    Oiseau o1 (3, 6); // d�clare un �lement de la classe Oiseau
    Personnage snoopy('S', 3, 50, 50, 10, 10, true); //d�clare un �l�ment de la classe Personnage
    //Balle b1 ('B', 10, 10, 10, 10); //d�clare un �l�ment de la classe Balle

    //snoopy.AffichagePersonnage(m_pConsole);
    int cpt = 0;
    bool pause =true;

    while(quit)
    {
        cpt++;
        cpt3++;
        if (cpt3>10)
        {
            cpt2=true;
            cpt3 = 0;
        }
        //else cpt2 = false;

        if (choc == true)///gestion du nombre de vie
            ///cpt2 permet d'�viter la perte de plusieurs vies lors d'une m�me collision
        {
            if (cpt2 == true)
            {

                if (m_nbVie==3 && choc==true)
                {
                    m_nbVie=2;
                    choc = false;
                }
                if (m_nbVie==2 && choc==true)
                {
                    m_nbVie=1;
                    choc = false;
                }
                if (m_nbVie==1 && choc==true)
                {
                    m_nbVie=0;
                    choc = false;
                }
                //cout<<m_nbVie;
                cpt2 = false;
            }
        }




        m_pConsole->gotoLigCol(0, 21);
        for(int i=0; i<3; i++) //initialisation
        {
            m_vie[i]=' ';
        }

        for(int i=0; i<m_nbVie; i++) //initialise les vies � 3
        {
            m_vie[i] = 3;
        }
        for(int i=0; i<3; i++)
        {
            m_pConsole->setColor(COLOR_RED); // affiche les vies sur la console
            cout <<m_vie[i];
        }
        m_pConsole->gotoLigCol(2, 21);
        m_pConsole->setColor(COLOR_YELLOW);
        cout<<"Nb Oiseau restant :"<<m_nbOiseau;

        if (pause)tempsfini=horloge(m_pConsole, tempsfini, compteur); ///fonction qui g�re le temps restant du niveau
        if (cpt >=4)///permet de rallentir le mouvement de la balle qui ne bouge que tous les 4 tours de while
        {
            if (pause) mat1.DeplacementBalle(m_pConsole);// g�re la balles
            cpt = 0;
        }
        m_pConsole->gotoLigCol(0, 0);
        mat1.AffichageMat(m_pConsole);// affiche la nouvelle matrice
        choc = mat1.CollisionBalle(choc);

        //pConsole->gotoLigCol(0, 0);
        m_pConsole->setColor(COLOR_WHITE);

        // Si on a appuy� sur une touche du clavier
        if (m_pConsole->isKeyboardPressed())
        {
            cpt++;
            cpt3++;
            // R�cup�re le code ASCII de la touche
            int key = m_pConsole->getInputKey();
            if (key == 'p'||key=='P')///Mode Pause
            {
                if (pause)
                {
                    cmp2 = ( (int) clock()/CLOCKS_PER_SEC);
                    pause = false;
                    ;
                }
                else
                {
                    int cmp = ( (int) clock()/CLOCKS_PER_SEC);
                    compteur = compteur + cmp - cmp2;//on garde en m�moire le temps de la pause
                    pause = true;

                }
            }
            if (pause)
            {

                if(key == 'S'||key=='s')sauvegarde(niv, mat1, m_nbOiseau, false);

                if(key == 27|| m_nbVie<=0 || tempsfini==true )//si on appuie sur �chap ou plus de vie ou temps �coul�
                {
                    quit = false;
                    m_pConsole->gotoLigCol(11, 0);
                    cout<<endl<<"Oups essaye encore!!!"<<endl; //fin de la partie
                    Sleep(1000);
                    cout<<"Retour au choix du niveau"<<endl;
                    m_victoire = false;
                }
                else
                {
                    mat1.DeplacementSnoopy(m_pConsole, key, m_nbOiseau2, m_nbVie2, crea);//g�re Snoopy
                }

            }
            if(m_nbOiseau==0) // si le joueur a r�cup�r� tous les oiseaux
            {
                quit=false;
                cout<<endl<<"C'est gagne !!!"<<endl;
                m_victoire =true; // Victoire du niveau vraie
            }
        }
        //while(choc ==true)
        Sleep(20);
    }

    //cout <<"Niveau2";
    return m_victoire; //retourne le bool�en victoire qui est vrai si le joueur a gagn�

}

bool Niveau::horloge(Console* pConsole, bool tempsfini, int compteur2)
{
    /// fonction qui compte et affiche le temps restant pour finir le niveau
    /// et retourne un bol�en qui vaut "vrai" lorsque le temps est �puis�
    int compteur;
    int tempsrestant;
    //nt compteur2;


    compteur = ( (int) clock()/CLOCKS_PER_SEC);//r�cup�re le temps en seconde depuis le lancement du JEU
    //compteur = compteur - compteur2;
    tempsrestant = (60 - compteur);// stocke la valeur du temps restant en secondes pour finir le niveau
    tempsrestant = (tempsrestant + compteur2);//on ajoute le temps entre le lancement du niveau et le lancement du jeu
    pConsole->gotoLigCol(4, 21); // postionne le curseur pour afficher

    if ((tempsrestant <10) && (tempsrestant >0)) // s'il reste moins de 10 secondes
    {
        pConsole->setColor(COLOR_RED);
        std::cout << "Temps restant:  "<< tempsrestant; // affiche le temps restant en rouge
    }

    if ((tempsrestant >= 10)&&(tempsrestant >0)) //s'il reste au moins 10 secondes
    {
        pConsole->setColor(COLOR_PURPLE);
        std::cout << "Temps restant: " << tempsrestant; // affiche le temps restant en violet
    }

    if (tempsrestant == 0 ) //s'il ne reste plus de temps pour finir le niveau
    {
        tempsfini=true;
    }
    m_timer = tempsrestant;
    //cout<<(*timer);
    return tempsfini; // retourne le bool�en qui indique s'il reste du temps( false) ou non (true)
}

void Niveau::sauvegarde(int niveau, Matrice mat, int nbOiseau, bool crea) ///Proc�dure pour sauvegarder
{
    system ("cls");
    string id;
    cout<<"Rentrez un nom d'utilisateur : ";
    cin>>id; //r�cup�re le nom du joueur
    ofstream objetfichier;
    string nomFichier = "sauvegarde/"+id+".txt";///On cr�e un fichier au nom de l'utilisateur
    objetfichier.open(nomFichier.c_str(), ios::out); //on ouvrre le fichier en ecriture
    if (objetfichier) //permet de tester si le fichier s'est ouvert sans probleme
    {
        objetfichier<<"niveau : "<<niveau<<endl;
        objetfichier<<nbOiseau<<endl;
        for (int i=0; i<10; i++)
        {
            for(int j=0; j<20; j++)
            {
                if (mat.getMatrice(i, j)==' ')objetfichier<<'x';//On remplace les espaces par des x pour que la r�cup�ration conserve ces espaces
                else objetfichier<<mat.getMatrice(i, j);

            }
            objetfichier<<endl;

            //cout <<CoordT(i, 0, 1)<<endl;
        }
        objetfichier<<crea;/// permet de savoir si la matrice provient d'un niveau cr�� par l'utilisateur
        objetfichier.close();

    }
    else cout<<"PROBLEME"; //blindage
    //objetfichier <<"contenu du fichier" << endl;//*
    //objetfichier.close(); //on ferme le fichier pour liberer la m�moire
    system("cls");

}

void Niveau::score( bool dernierNiveau, int niveau)
{///Fonction permettant de sauvegarder les scores ainsi que le calcul du score a chaque ,iveau
    system ("cls");

    int score = 0;
    score = m_timer *100;
    string id;
    cout<<"Rentrez un nom d'utilisateur : ";
    cin>>id; //r�cup�re le nom du joueur
    ofstream objetfichier3;
    string nomFichier = "score/"+id+".txt";
    objetfichier3.open(nomFichier.c_str(), ios::app); //on ouvrre le fichier en ecriture a la fin du fichier
    ofstream objetfichier4;
    objetfichier4.open("score/score.txt", ios::app);

    if (objetfichier3) //permet de tester si le fichier s'est ouvert sans probleme
    {

        objetfichier3<<"Niveau : "<<niveau<<" score : "<<score<<endl;
        cout<<"Votre score pour ce niveau est : "<<score<<endl;
        objetfichier4<<id<<" : "<<score<<endl;
        Sleep(2000);
    }
    else cout<<"PROBLEME"; //blindage
    objetfichier3.close();

    if (dernierNiveau == true)
    {
        ifstream objetfichier2;
        objetfichier2.open(nomFichier.c_str());//on ouvrre le fichier en ecriture a la fin du fichier
        string tmp;
        objetfichier2>>tmp;
        objetfichier2>>tmp;
        int score1, score2, score3;
        objetfichier2>>score1;
        objetfichier2>>tmp;
        objetfichier2>>tmp;
        objetfichier2>>score2;
        objetfichier2>>tmp;
        objetfichier2>>tmp;
        objetfichier2>>score3;
        int score4 = score1 + score2 + score3;///score4 correspond au score total apr�s avoir jou� aux 3 niveaux cons�cutifs
        cout<<"Votre score total est : "<<score4;
        objetfichier2.close();
        ofstream objetfichier;
        string nomFichier = "score/"+id+".txt";
        objetfichier.open(nomFichier.c_str(), ios::app); //on ouvrre le fichier en ecriture a la fin du fichier
        if (objetfichier) //permet de tester si le fichier s'est ouvert sans probleme
        {

            objetfichier<<"score final :"<<score4<<endl;
            Sleep(2000);
            //cout<<"Votre score pour ce niveau est : "<<score;
            //objetfichier<<nbOiseau<<endl;

        }

        else cout<<"PROBLEME";

    }

    //objetfichier <<"contenu du fichier" << endl;//*
    //objetfichier.close(); //on ferme le fichier pour liberer la m�moire
    system("cls");


}
void Niveau::editNiveau()
{///Fonction correspondant au mode Snoopy le constructeur
    system("cls");
    char tmp2 = 'A';
    Matrice mat1(4, 1);
    for (int i=0; i<10; i++)
    {
        for(int j=0; j<20; j++)
        {
            if(mat1.getMatrice(i,j)!='O'&&mat1.getMatrice(i,j)!='S'&&mat1.getMatrice(i,j)!='B')
                mat1.setMatrice(i, j, 'x');
        }
    }
    bool quit =true;
    while(quit)
    {


        m_pConsole->gotoLigCol(0, 0);
        mat1.AffichageMat(m_pConsole);// affiche la nouvelle matrice
        //pConsole->gotoLigCol(0, 0);
        m_pConsole->setColor(COLOR_WHITE);

        // Si on a appuy� sur une touche du clavier
        if (m_pConsole->isKeyboardPressed())
        {

            // R�cup�re le code ASCII de la touche
            int key = m_pConsole->getInputKey();

            //if(key == 'S'||key=='s')sauvegarde(niv, mat1, m_nbOiseau);

            if(key == 27)//si on appuie sur �chap
            {
                quit = false;

            }
            else
            {

                tmp2 = mat1.DeplacementSnoopy2(m_pConsole, key, tmp2); //g�re Snoopy
                //placement des diff�rents blocs a l'aide de la fonction DeplacementSnoopy2

            }

        }


    }
    sauvegarde(1, mat1, 4, true);




}

int Niveau::menu() ///Fonction qui affiche et g�re le menu principal
{
    system("cls");

    m_pConsole = NULL;
    bool quit = true;
    m_pConsole = Console::getInstance();
    m_pConsole->setColor(COLOR_DEFAULT);
Titre :
    ;

    cout << "      ____                                _      "<< endl;
    cout << "     / ___| _ __   ___   ___  _ __  _   _( )___ "<< endl;
    cout << "     \\___ \\| '_ \\ / _ \\ / _ \\| '_ \\| | | |// __|"<< endl;
    cout << "      ___) | | | | (_) | (_) | |_) | |_| | \\__ \\"<< endl;
    cout << "     |____/|_| |_|\\___/ \\___/| .__/ \\__, | |___/"<< endl;
    cout << "        |  _ \\ _____   _____ |_|_   |___/ ___   "<< endl;
    cout << "        | |_) / _ \\ \\ / / _ \\ '_ \\ / _` |/ _ \\  "<< endl;
    cout << "        |  _ <  __/\\ V /  __/ | | | (_| |  __/  "<< endl;
    cout << "        |_| \\_\\___| \\_/ \\___|_| |_|\\__, |\\___|  "<< endl;
    cout << "                                   |___/       \n\n\n"<< endl;
    cout <<"               Apppuyer sur Entree"<< endl;

    ///Ecran-titre
    while (quit)
    {

        if (m_pConsole->isKeyboardPressed())
        {
            // R�cup�re le code ASCII de la touche
            int key = m_pConsole->getInputKey();
            if (key == 13)
            {
                quit = false;
                system ("cls");
            }
            else
            {
                system ("cls");
                goto Titre;
            }

//        system("cls");

        }

    }

    quit = true;
    int choix =0;

choixA :
    ;
    cout<<endl;
    if (choix==0) // met en rouge la proposition sur laquelle se trouve le joueur
    {
        m_pConsole->setColor(COLOR_RED);
        cout<<"\tLancer une Partie"<<endl;
        m_pConsole->setColor(COLOR_DEFAULT);
        cout<<"\tReprendre une Partie"<<endl;
        cout<<"\tRegles"<<endl;
        cout<<"\tScore"<<endl;
        cout<<"\tSnoopy Le Constructeur"<<endl;
        cout<<"\tQuitter"<<endl;
    }
    if (choix==1) // 2eme ligne en rouge
    {
        m_pConsole->setColor(COLOR_DEFAULT);
        cout<<"\tLancer une Partie"<<endl;
        m_pConsole->setColor(COLOR_RED);
        cout<<"\tReprendre une Partie"<<endl;
        m_pConsole->setColor(COLOR_DEFAULT);
        cout<<"\tRegles"<<endl;
        cout<<"\tScore"<<endl;
        cout<<"\tSnoopy Le Constructeur"<<endl;
        cout<<"\tQuitter"<<endl;
    }
    if (choix==2) // 3eme ligne en rouge
    {
        m_pConsole->setColor(COLOR_DEFAULT);
        cout<<"\tLancer une Partie"<<endl;
        cout<<"\tReprendre une Partie"<<endl;
        m_pConsole->setColor(COLOR_RED);
        cout<<"\tRegles"<<endl;
        m_pConsole->setColor(COLOR_DEFAULT);
        cout<<"\tScore"<<endl;
        cout<<"\tSnoopy Le Constructeur"<<endl;
        cout<<"\tQuitter"<<endl;
    }
    if (choix==3) // 4eme ligne en rouge
    {
        m_pConsole->setColor(COLOR_DEFAULT);
        cout<<"\tLancer une Partie"<<endl;
        cout<<"\tReprendre une Partie"<<endl;
        cout<<"\tRegles"<<endl;
        m_pConsole->setColor(COLOR_RED);
        cout<<"\tScore"<<endl;
        m_pConsole->setColor(COLOR_DEFAULT);
        cout<<"\tSnoopy Le Constructeur"<<endl;
        cout<<"\tQuitter"<<endl;
    }
    if (choix==4) // 5eme ligne en rouge
    {
        m_pConsole->setColor(COLOR_DEFAULT);
        cout<<"\tLancer une Partie"<<endl;
        cout<<"\tReprendre une Partie"<<endl;
        cout<<"\tRegles"<<endl;
        cout<<"\tScore"<<endl;
        m_pConsole->setColor(COLOR_RED);
        cout<<"\tSnoopy Le Constructeur"<<endl;
        m_pConsole->setColor(COLOR_DEFAULT);
        cout<<"\tQuitter"<<endl;
    }
    if (choix==5) // 6eme ligne en rouge
    {
        m_pConsole->setColor(COLOR_DEFAULT);
        cout<<"\tLancer une Partie"<<endl;
        cout<<"\tReprendre une Partie"<<endl;
        cout<<"\tRegles"<<endl;
        cout<<"\tScore"<<endl;
        cout<<"\tSnoopy Le Constructeur"<<endl;
        m_pConsole->setColor(COLOR_RED);
        cout<<"\tQuitter"<<endl;
    }

    while (quit)
    {
        if (m_pConsole->isKeyboardPressed())
        {
            // R�cup�re le code ASCII de la touche
            int key = m_pConsole->getInputKey();
            if (key=='i'||key=='I') // si le joueur va vers le haut
            {
                if(choix>0)
                {
                    choix=choix-1;
                    system("cls");
                    goto choixA;
                }
            }
            if (key=='K'||key=='k') //si le joueur va vers le bas
            {
                if(choix<5)
                {
                    choix=choix+1;
                    system("cls");
                    goto choixA;

                }

            }
            if (key==13)
            {
                quit = false;
                //cout<<choix;
            }
        }
    }

    return choix; //retourne le choix du joueur
}

bool Niveau::menu2() ///fonction qui g�re le choix du niveau (nouvelle partie) et le menu entre les niveaux
{
    int position = 0;
choix:
    ;
    int choix = 0;
    int choix3 = 0;
    int choix2 = 0;
    bool quit = true;
    bool victoire = false;
    int choix4;
    if (position == 0)choix = menu();
    //if (position == 1)
    system("cls");

    if (choix==0)///Si lancer une nouvelle partie
    {
        //int choix2 = 0;
choixB :
        ;

        if (position == 1)goto choixC;
        if (position == 2)goto choixD;

        //cout<<choix2;
        cout<<endl;
        system("cls");
        if(choix2==0) //met en rouge la proposition ou se trouve le joueur
        {
            m_pConsole->setColor(COLOR_RED);
            cout<<"\tNiveau1"<<endl;
            m_pConsole->setColor(COLOR_DEFAULT);
            cout<<"\tNiveau2"<<endl;
            cout<<"\tNiveau3"<<endl;
            cout<<endl<<"retour"<<endl;
        }
        if (choix2==1) // 2eme en rouge
        {
            m_pConsole->setColor(COLOR_DEFAULT);
            cout<<"\tNiveau1"<<endl;
            m_pConsole->setColor(COLOR_RED);
            cout<<"\tNiveau2"<<endl;
            m_pConsole->setColor(COLOR_DEFAULT);
            cout<<"\tNiveau3"<<endl;
            cout<<endl<<"retour"<<endl;

        }
        if (choix2==2) //3eme en rouge
        {
            m_pConsole->setColor(COLOR_DEFAULT);
            cout<<"\tNiveau1"<<endl;
            cout<<"\tNiveau2"<<endl;
            m_pConsole->setColor(COLOR_RED);
            cout<<"\tNiveau3"<<endl;
            m_pConsole->setColor(COLOR_DEFAULT);
            cout<<endl<<"retour"<<endl;

        }
        if (choix2==3)
        {
            m_pConsole->setColor(COLOR_DEFAULT);
            cout<<"\tNiveau1"<<endl;
            cout<<"\tNiveau2"<<endl;
            cout<<"\tNiveau3"<<endl;
            m_pConsole->setColor(COLOR_RED);
            cout<<endl<<"retour"<<endl;

        }

        while (quit)
        {
            if (m_pConsole->isKeyboardPressed())
            {
                // R�cup�re le code ASCII de la touche
                int key = m_pConsole->getInputKey();

                if (key=='i'||key=='I') // si le joueur veut aller vers le haut
                {
                    if(choix2>0)
                    {
                        choix2=choix2-1;
                        system("cls");
                        goto choixB;
                    }
                }
                if (key=='k'||key=='K') //si le joueur veut aller vers le bas
                {
                    if(choix2<3)
                    {
                        choix2=choix2+1;
                        system("cls");
                        goto choixB;

                    }

                }
                if (key==13)
                {
                    quit = false;
                }
            }
        }
        //Niveau part1;
        victoire = false;

        if (choix2==0)
        {
            system("cls");
            victoire = jouer(1, NULL, 4, false); //lance la fonction qui g�re ici le niveau 1 et r�cup�re le boolen victoire
            if (victoire == 1) // si le joueur a gagn�
            {
                //cout<<(*timer);
                score( false, 1);

choixC :
                ;
                //int choix3 = 0;




                system("cls");
                m_pConsole->setColor(COLOR_DEFAULT);
                cout<<"Vous avez reussi avec brio le 1er niveau !"<<endl;

                if (choix3==0) //  Menu entre le niveau 1 et 2
                {
                    m_pConsole->setColor(COLOR_RED);
                    cout<<"\tDirection le niveau 2"<<endl;
                    m_pConsole->setColor(COLOR_DEFAULT);
                    cout<<"\tRetour au menu"<<endl;
                }
                if (choix3==1)
                {
                    m_pConsole->setColor(COLOR_DEFAULT);
                    cout<<"\tDirection le niveau 2"<<endl;
                    m_pConsole->setColor(COLOR_RED);
                    cout<<"\tRetour au menu"<<endl;
                }
                bool quit = true;
                while (quit)
                {
                    if (m_pConsole->isKeyboardPressed())
                    {
                        // R�cup�re le code ASCII de la touche
                        int key = m_pConsole->getInputKey();

                        if (key=='i'||key=='I') // si le joueur veut aller en haut
                        {
                            if(choix3==1)
                            {
                                choix3=0;
                                system("cls");
                                goto choixC;
                            }
                        }
                        if (key=='k'||key=='K') // si le joueur veut aller en bas
                        {
                            if(choix3==0)
                            {
                                choix3=1;
                                system("cls");
                                goto choixC;

                            }

                        }
                        if (key==13)
                        {
                            quit = false;
                        }
                    }
                }
                m_victoire=false;
                if (choix3==0)choix2 = 1;
                if (choix3==1)
                {
                    goto choix;
                    m_victoire =false; //initialise victoire
                }

            }
            else
            {
                quit = true;
                goto choixB;
            }
        }

        if (choix2==1)
        {

            system("cls");
            m_victoire = jouer(2, NULL, 4, false); //fonction qui g�re le niveau 2 et retourne le bool�en victoire
            if (m_victoire == 1) // s'il a gagn�
            {
                score( false, 2);
                choix4 = 0;
choixD : // affiche le menu entre le niveau 2 et niveau 3
                ;

                system("cls");
                m_pConsole->setColor(COLOR_DEFAULT);
                cout<<"Vous avez reussi avec brio le 2nd niveau !"<<endl;

                if (choix4==0) // si cureur sur la 1ere proposition, elle se met en rouge
                {
                    m_pConsole->setColor(COLOR_RED);
                    cout<<"\tDirection le niveau 3"<<endl;
                    m_pConsole->setColor(COLOR_DEFAULT);
                    cout<<"\tRetour au menu"<<endl;
                }
                if (choix4==1) // si cureur sur la 2eme proposition, elle se met en rouge
                {
                    m_pConsole->setColor(COLOR_DEFAULT);
                    cout<<"\tDirection le niveau 3"<<endl;
                    m_pConsole->setColor(COLOR_RED);
                    cout<<"\tRetour au menu"<<endl;
                }
                bool quit = true;
                while (quit)
                {
                    if (m_pConsole->isKeyboardPressed())
                    {
                        // R�cup�re le code ASCII de la touche
                        int key = m_pConsole->getInputKey();

                        if (key=='i'||key=='I') //si le joueur va vers le haut
                        {
                            if(choix4==1)
                            {
                                choix4=0;
                                system("cls");
                                goto choixD;
                            }
                        }
                        if (key=='K'||key=='k') // si le joueur va vers le bas
                        {
                            if(choix4==0)
                            {
                                choix4=1;
                                system("cls");
                                goto choixD;

                            }

                        }
                        if (key==13)
                        {
                            quit = false;
                        }
                    }
                }
                m_victoire=false; //initialise le bool�en victoire

                if (choix4==0)
                    choix2 = 2;
                if (choix4==1)
                {
                    goto choix;
                    //m_victoire =false;
                }

            }
            else
            {
                quit = true;
                goto choixB;
            }
        }

        if (choix2==2)
        {
            system("cls");
            m_victoire = jouer(3, NULL, 4, false); //fonction qui g�re le niveau 3 et retourne le bool�en victoire
            if (m_victoire == true) //s'il a gagn�, fin du jeu
            {
                score( true, 3);
                cout<<"FELICITATIONS VOUS AVEZ FINI LE JEU"<< endl<<endl;
                //goto choix;
            }
            else
            {
                quit = true;
                goto choixB;
            }
        }
        if (choix2==3)goto choix;
    }

    if (choix==1)///Si Reprendre une partie
    {
retour :
        ;
        string id;
        int rep;
        bool crea = false;
        cout<<"Rentrez votre nom d'utilisateur : ";
        cin>>id;

        string nomFichier ="sauvegarde/"+id+".txt";
        ifstream monFlux(nomFichier.c_str());
//D�claration d'un flux permettant d'�crire dans un fichier.

        if (monFlux)//R�cup�ration de la matrice dans un fichier du rep sauvegarde
        {
            bool victoire = false;
            int nbOiseau;
            char niveau2;
            string niveau;
            monFlux>>niveau;
            monFlux>>niveau2;
            int niv;
            monFlux>>niv;
            //cout<<niv;
            monFlux>>nbOiseau;
            Matrice mat(4, niv);
            Matrice* mat1 = &mat;
            char a;
            for (int i=0; i<10; i++)
            {
                for (int j=0; j<20; j++)
                {
                    monFlux>>a;
                    mat.setMatrice(i, j, a);
                    if (a == 'x'||a=='S' ||a == 'B') mat.setMatrice(i, j, ' ');
                }
            }
            monFlux>>crea;
            //cout<<crea;
            system("cls");
            victoire = jouer(niv, mat1, nbOiseau, crea);
            if ((victoire==true)&&(niv==1))
            {
                goto choixC;
                position = 1;
            }
            if ((victoire == true)&&(niv==2))
            {
                position = 2;
                goto choixD;
            }
            else if(victoire == false) goto choixB;
        }
        else //blindage
        {

            int chois = 0;
choix24 :
            ;
            if (chois == 0)
            {
                m_pConsole->setColor(COLOR_DEFAULT);
                cout<<"pas de sauvegarde correspondante"<<endl;
                m_pConsole->setColor(COLOR_RED);
                cout<<"Reessayer"<<endl;
                m_pConsole->setColor(COLOR_DEFAULT);
                cout<<"Retour menu"<<endl;
            }
            if (chois==1)
            {
                m_pConsole->setColor(COLOR_DEFAULT);
                cout<<"pas de sauvegarde correspondante"<<endl;
                m_pConsole->setColor(COLOR_DEFAULT);
                cout<<"Reessayer"<<endl;
                m_pConsole->setColor(COLOR_RED);
                cout<<"Retour menu"<<endl;
            }
            bool quit = true;
            while (quit)
            {
                if (m_pConsole->isKeyboardPressed())
                {
                    // R�cup�re le code ASCII de la touche
                    int key = m_pConsole->getInputKey();

                    if (key=='i'||key=='I') // si le joueur veut aller en haut
                    {
                        if(chois==1)
                        {
                            chois=0;
                            system("cls");
                            goto choix24;
                        }
                    }
                    if (key=='k'||key=='K') // si le joueur veut aller en bas
                    {
                        if(chois==0)
                        {
                            chois=1;
                            system("cls");
                            goto choix24;

                        }

                    }
                    if (key==13)
                    {
                        quit = false;
                    }
                }
            }


            if (chois == 0) goto retour;
            if (chois == 1) goto choix;

        }
    }
    if (choix == 2)///Regles
    {
        system("cls");
        cout<<"\tRegles "<<endl;
        cout<<"Le but est que snoopy mange les 4 oiseaux en moins de 60 secondes."<<endl;
        cout<<"Les deplacements se font avec les touches I, J, K et L."<<endl;
        cout<<"Les collisions avec les T et la Balle font perdre des vies."<<endl;
        cout<<"Les blocs P sont poussables une seule fois, les blocs C sont cassables"<<endl;
        cout<<"avec la touche C. La touche S permet de sauvegarder un niveau."<<endl;
        cout<<"GOOD LUCK !"<<endl;
        m_pConsole->setColor(COLOR_RED);
        cout<<endl<<"retour"<<endl;
        bool quit = true;
        while (quit)
        {
            if (m_pConsole->isKeyboardPressed())
            {
                // R�cup�re le code ASCII de la touche
                int key = m_pConsole->getInputKey();
                if (key==13)goto choix;
            }
        }

    }
    if (choix == 3)///Score
    {
        system("cls");
        ifstream objetfichier;
        objetfichier.open("score/score.txt");
        if (objetfichier)
        {
            string ligne;
            m_pConsole->setColor(COLOR_DEFAULT);
            cout <<"Score precedent "<<endl<<endl;
            while(getline(objetfichier, ligne)) //Tant qu'on n'est pas � la fin, on lit
            {

                cout <<"\t"<<ligne << endl;
                //Et on l'affiche dans la console
                //Ou alors on fait quelque chose avec cette ligne
                //� vous de voir
            }
            m_pConsole->setColor(COLOR_RED);
            cout<<endl<<"retour"<<endl;
            bool quit = true;
            while (quit)
            {
                if (m_pConsole->isKeyboardPressed())
                {
                    // R�cup�re le code ASCII de la touche
                    int key = m_pConsole->getInputKey();
                    if (key==13)goto choix;
                }
            }


        }
    }
    if (choix == 4)///Snoopy le Constructeur
    {
        editNiveau();
        goto choix;
    }
    if (choix==5)///Quitter
        exit(EXIT_FAILURE);

}


Niveau::~Niveau()
{
    //dtor
}

