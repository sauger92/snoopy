#include "Personnage.h"
#include "console.h"

Personnage::Personnage()//Default constructor
    :m_lettre('S'),m_vies(3),m_pos_x(5),m_pos_y(10),m_dep_x(),m_dep_y(),m_vivant(true)
{
}

Personnage::Personnage(char _lettre, int _vies, int _pos_x, int _pos_y, int _dep_x, int _dep_y, bool _vivant )//Overload constructor
    :m_lettre(_lettre), m_vies(_vies), m_pos_x(_pos_x), m_pos_y(_pos_y), m_dep_x(_dep_x), m_dep_y(_dep_y), m_vivant(_vivant)
{
}

Personnage::~Personnage()//Destructor
{
}

///Getter
char Personnage::getLettre() const
{
    return m_lettre;
}

int Personnage::getVies() const
{
    return m_vies;
}

int Personnage::getPos_x() const
{
    return m_pos_x;
}

int Personnage::getPos_y() const
{
    return m_pos_y;
}

int Personnage::getDep_x() const
{
    return m_dep_x;
}

int Personnage::getDep_y() const
{
    return m_dep_y;
}

bool Personnage::getVivant() const
{
    return m_vivant;
}

///Setter
void Personnage::setLettre(char lettre)
{
    m_lettre = lettre;
}

void Personnage::setVies(int vies)
{
    m_vies = vies;
}

void Personnage::setPos_x(int pos_x)
{
    m_pos_x = pos_x;
}

void Personnage::setPos_y(int pos_y)
{
    m_pos_y = pos_y;
}

void Personnage::setDep_x(int dep_x)
{
    m_dep_x = dep_x;
}

void Personnage::setDep_y(int dep_y)
{
    m_dep_y = dep_y;
}

void Personnage::setVivant(bool vivant)
{
    m_vivant = vivant;
}

///M�thodes
void Personnage::AffichagePersonnage(Console* pConsole) // Proc�dure pour afficher la lettre du personnage
{
    pConsole->gotoLigCol(getPos_x(), getPos_y()); //r�cup�re les coordonn�es
    pConsole->setColor(COLOR_WHITE); // Choisi la couleur de la lettre
    std::cout << getLettre(); //affiche la lettre selon les coordonn�es r�cup�r�es avec la couleur choisie
}
